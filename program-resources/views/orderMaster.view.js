sap.ui.jsview("program-resources.views.orderMaster", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf mvc.MVC
	*/ 
	getControllerName : function() {
		return "program-resources.controller.orderMaster";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf mvc.MVC
	*/ 
	createContent : function(oController) {
		
		// create notification list
		var list = new sap.m.List(this.createId("orderList"), {
			mode: jQuery.device.is.phone ? sap.m.ListMode.None : sap.m.ListMode.SingleSelectMaster,
			select : [oController.onOrderSelect, oController]
		});
		
		// create searchfield
		var searchField = new sap.m.SearchField("orderSearch", {
			placeholder: "Search...",
			search: [oController.onSearch, oController],
			liveChange: [oController.onLiveChange, oController]
		});
		
		// page footer
		var pageFooter = new sap.m.Bar({});
		
		
		return new sap.m.Page({
			title: "Orders",
			showNavButton : true,
			navButtonPress: function(){
				oSplitApp.backToTopMaster(); // when pressed, the back button should navigate back up to page 1
		    },
			content: [searchField, list],
			footer : pageFooter
		});	
	}
});