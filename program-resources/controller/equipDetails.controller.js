function handleImageClick(oEvent) {
	alert("image clicked");
}

/**
 * Converts date in an ABAP conform format
 * @param date -- date string
 * @returns {String} -- ABAP conform date string
 */
function abapDateEngl(date) {
	var oArray = date.split(' ');
	var day = oArray[1].substring(0,2);
	var selMonth;
	var months = [
			{month : 'Jan', value : '01'}, {month : 'Feb', value : '02'}, {month : 'Mar', value : '03'}, {'Apr' : '04'},
			{month : 'May', value : '05'}, {month : 'Jun', value : '06'}, {month : 'Jul', value : '07'}, {'Aug' : '08'},
			{month : 'Sep', value : '09'}, {month : 'Oct', value : '10'}, {month : 'Nov', value : '11'}, {'Dec' : '12'}];
	
	for(var i = 0; i < months.length; i++) {
		if(months[i].month == oArray[0]) {
			selMonth = months[i].value;			
		}
	}

	return oArray[2] + '-' + selMonth + '-' + day + 'T00:00:00';
}

/**
 * Converts date in an ABAP conform format
 * @param date -- date string
 * @returns {String} -- ABAP conform date string
 */
function abapDate(date) {
	if(date == '') {
		return "2014-10-10T00:00:00";
	} else {
		var oArray = date.split('.');
		return oArray[2] + '-' + oArray[1] + '-' + oArray[0] + 'T00:00:00';		
	}
}

/**
 * Converts time in an ABAP conform format
 * @param time -- time string
 * @returns {String} -- ABAP conform time string
 */
function abapTimeEngl(time) {
	var oArray = time.split(':');
	var minutes = oArray[1].split(' ');
	return 'PT' + oArray[0] + 'H' + minutes[0] + 'M00S';
}

/**
 * Converts time in an ABAP conform format
 * @param time -- time string
 * @returns {String} -- ABAP conform time string
 */
function abapTime(time) {
	if(time == '') {
		return "PT00H00M00S";
	} else {
		var oArray = time.split(':');
		return 'PT' + oArray[0] + 'H' + oArray[1] + 'M00S';		
	}
}

/**
 * Function for getting informations from Edit fields and building a json object
 * @returns json -- data object in json format
 */
function buildJson(notifnr) {
	var json = {
			'funcloc' : document.getElementById("equipDetails--funclocField-inner").value,
			'equipName' : document.getElementById("equipDetails--equipName-inner").value,
			'equipment' : document.getElementById("equipDetails--equipField-inner").value,
			'shortTxt' : document.getElementById("equipDetails--shtxtField-inner").value,
			'date' : abapDate(document.getElementById("equipDetails--datePicker-inner").value),
			'time' : abapTime(document.getElementById("equipDetails--timePicker-inner").value),
			'longTxt' : document.getElementById("equipDetails--dmgField-inner").value,
			'prio' : sap.ui.getCore().byId('prioField').getSelectedKey(),
			'qmnum' : notifnr
	};
	
	return json;
}

/**
 * Formats the data for the ajax request, Includes the corresponding service information in the request header
 * @param dataSet -- json Object with request data
 * @returns {String} -- data with request header
 */
function buildRequestData(dataSet) {
	
	// set some variables that are not in the attribute set
	obj.meldung = new Object();
	obj.meldung.equipname = dataSet.equipName;
	obj.meldung.longtext = dataSet.longtext;
	
	var reqHeader = 	'<?xml version="1.0" encoding="utf-8"?>' +
	 					'<entry xmlns="http://www.w3.org/2005/Atom" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices" xml:base="http://p761.coil.sap.com:50000/com.orianda.logon/NotifSet">' +
	 					'<category term="Z_MARENCO_01_SRV.FuncLoc" scheme="http://schemas.microsoft.com/ado/2007/08/dataservices/scheme"/>' +
	 					'<content type="application/xml">';

	var reqProperties = '<m:properties>' +
						'<d:Tplnr>' + dataSet.funcloc + '</d:Tplnr>' +
			            '<d:Erzeit>' + dataSet.time + '</d:Erzeit>' +
			            '<d:Erdat>' + dataSet.date + '</d:Erdat>' +
			            '<d:Priok>' + dataSet.prio + '</d:Priok>' +
			            '<d:Qmtxt>' + dataSet.equipName + ':' +dataSet.shortTxt + '</d:Qmtxt>' +
			            '<d:Equnr>' + dataSet.equipment + '</d:Equnr>' +
			            '<d:Qmnum>' + dataSet.qmnum + '</d:Qmnum>' +
			            '</m:properties>';					 	
					 	
	var reqFooter = 	'</content>' +
	 					'</entry>';
	
	return reqHeader + reqProperties + reqFooter;					 	
}

/**
 * function for clearing Formelements
 */
function clear() {
	 document.getElementById("equipDetails--shtxtField-inner").value = "";
	 document.getElementById("equipDetails--datePicker-inner").value = "";
	 document.getElementById("equipDetails--timePicker-inner").value = "";
	 document.getElementById("equipDetails--dmgField-inner").value = "";
	 var prio = sap.ui.getCore().byId('prioField');
	 prio.setSelectedKey('0');
}

/**
 * function for reloading dataset after creating new notification
 */
function reloadData(notifnr) {
	$.ajax({
		type: 'GET',
		url: obj.applicationContext.applicationEndpointURL + "/FuncLocSet('"+ obj.tplnr +"')?$expand=Equis,SubFuncLocs/SubFuncLocs/SubFuncLocs,MesPnts/MesDocs,Notifs,Orders/Operas&$format=json",
		headers: {
			"Content-Type" : "application/atom+xml;charset=utf-8",
			"DataServiceVersion" : "2.0",
			"X-CSRF-Token" : "fetch",
			"X-SMP-APPCID" : obj.applicationContext.applicationConnectionId
		},
		beforeSend : function(xhr) {
			// crypto call before authentication
			var bytes = Crypto.charenc.Binary.stringToBytes(obj.applicationContext.registrationContext.user + ":" + obj.applicationContext.registrationContext.password);
			var base64 = Crypto.util.bytesToBase64(bytes);
			//build request header
			xhr.setRequestHeader("Authorization", "Basic " + base64);
		},
		success: function(data, status, xhr) {
			var header_xcsrf_token = xhr.getResponseHeader('x-csrf-token');
			obj.tkn = header_xcsrf_token;
			obj.data = data;
			obj.reload = true;
			
			//hide busy indicator
			var busy = sap.ui.getCore().byId('eqBusy');
			busy.setVisible(false);
			busy.setBusy(false);
			
			// notif user
			sap.m.MessageToast.show("Created new Notification: "+ notifnr, { 
				duration : 500,
				my : "center",
				at : "center"
			});
			
			oSplitApp.backDetail();
			oSplitApp.backMaster();			
			// set position back to tiles and set cam element initial 
			crazyArchitectWorld.removeModel(obj.data.d);

		},
		error: function(jqXHR, textStatus, errorThrown){
			var error = jqXHR.responseXML.getElementsByTagName("message")[0] != null ? jqXHR.responseXML.getElementsByTagName("message")[0].textContent : "No error text defined";
			sap.m.MessageToast.show("Error reloading Data:" + error, { 
				duration : 5000,
				my : "center",
				at : "center"
			});
		}
	});
}

// controller instance
sap.ui.controller("program-resources.controller.equipDetails", {
	
	/*
	 * Function for filling formelements for the notification
	 */
	oFillForm: function() {
		this.byId("funclocField").setValue(obj.tplnr);
		this.byId("equipField").setValue(obj.equnr);
		this.byId("equipName").setValue(obj.eqShtxt);	
	},
	
	/*
	 * Function for saving notification
	 */
	oSaveNotification: function(oEvent) {
		// get content from control fields
		var json = buildJson("");
		var raw = buildRequestData(json);
		
		// show busy indicator
		var busy = sap.ui.getCore().byId('eqBusy');
		busy.setVisible(true);
		busy.setBusy(true);
		
		$.ajax({
			type: 'POST',
			data: raw,
			url: obj.applicationContext.applicationEndpointURL + "/NotifSet",
			headers: {
				"Content-Type" : "application/atom+xml;charset=utf-8",
				"DataServiceVersion" : "2.0",
				"X-CSRF-Token" : obj.tkn,
				"X-SMP-APPCID" : obj.applicationContext.applicationConnectionId
			},
			beforeSend : function(xhr) {
				// crypto call before authentication
				var bytes = Crypto.charenc.Binary.stringToBytes(obj.applicationContext.registrationContext.user + ":" + obj.applicationContext.registrationContext.password);
				var base64 = Crypto.util.bytesToBase64(bytes);
				//build request header
				xhr.setRequestHeader("Authorization", "Basic " + base64);
			},
			success: function(data, status, xhr) {
				var tmp = data;
				clear();
				document.getElementById('initDetails--initPage-title').innerHTML = obj.tplnr;
				// reload Notifs 
				reloadData(data.childNodes[0].children[5].childNodes[0].childNodes[15].textContent);
			},
			error: function(jqXHR, textStatus, errorThrown){
				var error = jqXHR.responseXML.getElementsByTagName("message")[0] != null ? jqXHR.responseXML.getElementsByTagName("message")[0].textContent : "No error text defined";
				sap.m.MessageToast.show("Error creating Notification:" + error, { 
					duration : 5000,
					my : "center",
					at : "center"
				});
			}
		});		
	},
		
	/*
	 * function for canceling notification and for back-navigation
	 */
	oCancelNotification: function() {
		clear();
		oSplitApp.backDetail();
		oSplitApp.backMaster();		
		crazyArchitectWorld.removeModel(obj.data.d);
		sap.ui.getCore().byId("__page1-navButton").setVisible(true);
	},

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf mvc.MVC
*/
	onInit: function() {
        this.getView().addDelegate({ 
        	onAfterShow: function(evt) {
        		$("#application").css({ 'background' : '' });
        		$("#application-BG").css({ 'opacity': '1' });
        		//oSplitApp.showMaster();	
        	}
        }); 
	},
});