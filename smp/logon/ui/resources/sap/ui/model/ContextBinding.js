/*!
 * SAP UI development toolkit for HTML5 (SAPUI5)
 * 
 * (c) Copyright 2009-2013 SAP AG. All rights reserved
 */
jQuery.sap.declare("sap.ui.model.ContextBinding");jQuery.sap.require("sap.ui.model.Binding");sap.ui.model.Binding.extend("sap.ui.model.ContextBinding",{constructor:function(m,p,c,P,e){sap.ui.model.Binding.call(this,m,p,c,P,e);this.bInitial=true},metadata:{publicMethods:["getElementContext"]}});
sap.ui.model.ContextBinding.prototype.checkUpdate=function(f){};
sap.ui.model.ContextBinding.prototype.getBoundContext=function(c){return this.oElementContext};
