var bytes = Crypto.util.base64ToBytes(localStorage.getItem('object'));
var string = Crypto.charenc.Binary.bytesToString(bytes);
obj = JSON.parse(string);
obj.longtext, obj.eqname, obj.material;
obj.inEquips = false;
obj.reload = false;
obj.data = null;
obj.tkn = null;
obj.funcLocs = null;

/**
 * function for getting object position
 * @param substrings
 * @returns
 */
function getObjectPosition(substrings) {
	var number = null;
	for(var i = 0; i < substrings.length; i++) {
		var tmp = parseInt(substrings[i]);
		if(isNaN(tmp)) {
			continue;
		} else {
			number = tmp;
			break;
		}
	}	
	return number;
}

//function getFunclocsWithMatpics() {
//	obj.flPicArray = new Object();
//	var position = 0;
//	for(var i = 0; i < obj.data.d.Equis.results.length; i++) {
//		var tmp = obj.data.d.Equis.results[i].Matnr;
//		if(tmp == '1200-1002487-AB' || tmp == '1200-1009429-AA' || tmp == '1200-1009458-AA' || tmp == '1200-1014493-AA' || tmp == '1200-1014495-AA' || tmp == '1200-1014496-AA' || tmp == '1200-1014765-AA' || tmp == '1200-1014850-AA') {
//			obj.flPicArray[position] = {"Equnr":obj.data.d.Equis.results[i].Equnr, "Matnr":obj.data.d.Equis.results[i].Matnr};
//			position++;
//		}
//	}
//}

var crazyArchitectWorld = {
	loaded: false,
	rotating: false,	
	tracker: null,
	trackables : new Array(),
	//tilesVisible : true,
	//equiTrackable : null,

	init: function initFn() {
		// To receive a notification once the image target is inside the field of vision the onEnterFieldOfVision trigger of the AR.Trackable2DObject is used. In the example the function appear() is attached. Within the appear function the previously created AR.AnimationGroup is started by calling its start() function which plays the animation once.
		// To add the AR.ImageDrawable to the image target together with the 3D model both drawables are supplied to the AR.Trackable2DObject.
		this.tracker = new AR.Tracker("assets/marenco.wtc", {
			//onLoaded: this.loadingStep
		});
		AR.context.services.sensors = false;
		this.getFuncLocs();
	},
	initTracker: function (funcLoc) {
		this.trackables[funcLoc.Functlocation] = new AR.Trackable2DObject(this.tracker, funcLoc.Functlocation, {
				// initialise cam element
			    drawables: {
			        cam: null
			    },
				onEnterFieldOfVision: function() {
					// Lesen des Technischen Platzes sofern nicht bereits gelesen
					if (obj.data === null ||  obj.data.d === undefined || obj.data.d.Functlocation != funcLoc.Functlocation){
						crazyArchitectWorld.getSubFuncLocs(funcLoc.Functlocation);	

					}	
					// Sofern Model VideoDrawable ist dann soll dieses weiterabgespielt werden
					if( crazyArchitectWorld.model instanceof AR.VideoDrawable ){
						crazyArchitectWorld.model.resume( );
					}
				},
				onExitFieldOfVision: function() {
					// Pause bei VideoDrawables
					if( crazyArchitectWorld.model instanceof AR.VideoDrawable ){
						crazyArchitectWorld.model.pause( );
					}
				}
			});		
	},

	removeModel: function(funcLoc){
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && this.model != null ){			
			this.trackables[funcLoc.Functlocation].drawables.removeCamDrawable(this.model);
			this.model = null;
		}
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && ( this.filled != 'undefined' || this.filled != null ) && this.filled instanceof AR.ImageDrawable){
			this.filled.opacity = 0;
		}	
			
	},
	showVideo: function( funcLoc ){
		this.removeModel(funcLoc);
		this.model = new AR.VideoDrawable("assets/assemble_fuel_sensor_comp.mp4", 0.5, {
		    offsetX: 0.2,
		    offsetY: 0.2,
		    zOrder: 3
		});
		this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.model);
		this.model.play();
	},
	showOutLine: function( funcLoc ) {	
		this.outLine = new AR.ImageDrawable(new AR.ImageResource("assets/" + funcLoc.Functlocation + "_outline.png"), 1, { zOrder: 0} );
		this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.outLine);		
	},
	removeOutLine: function( funcLoc ){
//		Remove Outline
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && this.outLine != null ){
			this.trackables[funcLoc.Functlocation].drawables.removeCamDrawable(this.outLine);
			this.outLine = null;
		}
//		Remove Filled Image
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && this.filled != null ){
			this.trackables[funcLoc.Functlocation].drawables.removeCamDrawable(this.filled);
			this.filled = null;
		}
	},
	showMaterial: function(funcLoc) {
		this.removeModel(funcLoc);
//		Set Filled
		if( ( this.filled != 'undefined' || this.filled != null ) && this.filled instanceof AR.ImageDrawable){
			this.filled.opacity = 1;
		}else{
			this.filled = new AR.ImageDrawable(new AR.ImageResource("assets/" + funcLoc.Functlocation + "_filled.png"), 1, {zOrder: 1} );
			this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.filled);	
		}
//		Create Model
		var position = getObjectPosition(obj.selPath.split("/"));
		this.model = new AR.Model("assets/"+ obj.data.d.Equis.results[position].Matnr +".wt3", {			
			//	The drawables are made clickable by setting their onClick triggers. Click triggers can be set in the options of the drawable when the drawable is created. Thus, when the 3D model onClick: this.toggleAnimateModel is set in the options it is then passed to the AR.Model constructor. Similar the button's onClick: this.toggleAnimateModel trigger is set in the options passed to the AR.ImageDrawable constructor. toggleAnimateModel() is therefore called when the 3D model or the button is clicked.
			//	Inside the toggleAnimateModel() function, it is checked if the animation is running and decided if it should be started, resumed or paused.
			onClick: this.toggleAnimateModel,
			scale: {
				x: 0.01,
				y: 0.01,
				z: 0.01
			},
			translate: {
				x: -0.1,
				y: -0.05,
				z: 0
			},
			rotate: {
				tilt : 270, 
				heading: 260, 
				roll: -18 
			},
			zOrder: 2		
		});
		console.log("assets/"+ obj.data.d.Equis.results[position].Matnr +".wt3");
		this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.model);
		// As a next step, an appearing animation is created. For more information have a closer look at the function implementation.
		this.appearingAnimation = this.createAppearingAnimation(this.model, 0.015);
		// The rotation animation for the 3D model is created by defining an AR.PropertyAnimation for the rotate.roll property.
		this.rotationAnimation =  new AR.PropertyAnimation( this.model, "rotate.heading", 0, 359, 10000 );
		this.appear();
	},

	createAppearingAnimation: function createAppearingAnimationFn(model, scale) {
		//	The animation scales up the 3D model once the target is inside the field of vision. Creating an animation on a single property of an object is done using an AR.PropertyAnimation. Since the car model needs to be scaled up on all three axis, three animations are needed. These animations are grouped together utilizing an AR.AnimationGroup that allows them to play them in parallel.
		//	Each AR.PropertyAnimation targets one of the three axis and scales the model from 0 to the value passed in the scale variable. An easing curve is used to create a more dynamic effect of the animation.
		var sx = new AR.PropertyAnimation(model, "scale.x", 0, scale, 2000, {
			type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
		});
		var sy = new AR.PropertyAnimation(model, "scale.y", 0, scale, 2000, {
			type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
		});
		var sz = new AR.PropertyAnimation(model, "scale.z", 0, scale, 2000, {
			type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
		});

		return new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, [sx, sy, sz]);
	},

	appear: function appearFn() {
		// Resets the properties to the initial values.
		crazyArchitectWorld.resetModel();
		crazyArchitectWorld.appearingAnimation.start();
		this.toggleAnimateModel( );
	},

	resetModel: function resetModelFn() {
		crazyArchitectWorld.rotationAnimation.stop();
		crazyArchitectWorld.rotating = false;
		crazyArchitectWorld.model.rotate.roll = -25;
	},

	toggleAnimateModel: function toggleAnimateModelFn() {
		if (!crazyArchitectWorld.rotationAnimation.isRunning()) {
			if (!crazyArchitectWorld.rotating) {
				// Starting an animation with .start(-1) will loop it indefinitely.
				crazyArchitectWorld.rotationAnimation.start(-1);
				crazyArchitectWorld.rotating = true;
			} else {
				// Resumes the rotation animation
				crazyArchitectWorld.rotationAnimation.resume();
			}
		} else {
			// Pauses the rotation animation
			crazyArchitectWorld.rotationAnimation.pause();
		}

		return false;
	},
	// Lesen des obersten technischen Platzes
	getFuncLocs:function getFuncLocsFn(){
		$.ajax({
			type: 'GET',
			url: obj.applicationContext.applicationEndpointURL + "/FuncLocSet('HELICOPTER')?$expand=SubFuncLocs&$format=json",
			headers: {
				"Content-Type" : "application/atom+xml;charset=utf-8",
				"DataServiceVersion" : "2.0",
				"X-CSRF-Token" : "fetch",
				"X-SMP-APPCID" : obj.applicationContext.applicationConnectionId
			},
			beforeSend : function(xhr) {
				// crypto call before authentication
				var bytes = Crypto.charenc.Binary.stringToBytes(obj.applicationContext.registrationContext.user + ":" + obj.applicationContext.registrationContext.password);
				var base64 = Crypto.util.bytesToBase64(bytes);
				//build request header
				xhr.setRequestHeader("Authorization", "Basic " + base64);
			},
			success: function(data, status, xhr) {
				var header_xcsrf_token = xhr.getResponseHeader('x-csrf-token');
				obj.tkn = header_xcsrf_token;
				// container for available funclocs
				obj.funcLocs = data;
				// set position to tiles
				obj.position = "tiles";
				obj.funcLocs.d.SubFuncLocs.results.forEach(function(funcLoc){
					crazyArchitectWorld.initTracker(funcLoc);
				});
			},
			error: function(jqXHR, textStatus, errorThrown){
				sap.m.MessageToast.show("Erro loading Funcloc: " + textStatus + ", " + errorThrown, { 
					duration : 5000,
					my : "center",
					at : "center"
				});
			}
		})
	},
	
	getSubFuncLocs: function getSubFuncLocsFn(funcLoc) {
		console.log(funcLoc);
		obj.tplnr = funcLoc;
//		Anzeige der Outline
		var dummy = {Functlocation : funcLoc };
		this.showOutLine( dummy );		
		// set busy indicator visible
		var busy = sap.ui.getCore().byId('flBusy');
		busy.setVisible(true);
		busy.setBusy(true);
		
		$.ajax({
			type: 'GET',
			url: obj.applicationContext.applicationEndpointURL + "/FuncLocSet('"+ funcLoc +"')?$expand=Equis,SubFuncLocs/SubFuncLocs/SubFuncLocs,MesPnts/MesDocs,Notifs,Orders/Operas&$format=json",
			headers: {
				"Content-Type" : "application/atom+xml;charset=utf-8",
				"DataServiceVersion" : "2.0",
				"X-CSRF-Token" : "fetch",
				"X-SMP-APPCID" : obj.applicationContext.applicationConnectionId
			},
			beforeSend : function(xhr) {
				// crypto call before authentication
				var bytes = Crypto.charenc.Binary.stringToBytes(obj.applicationContext.registrationContext.user + ":" + obj.applicationContext.registrationContext.password);
				var base64 = Crypto.util.bytesToBase64(bytes);
				//build request header
				xhr.setRequestHeader("Authorization", "Basic " + base64);
			},
			success: function(data, status, xhr) {
				var header_xcsrf_token = xhr.getResponseHeader('x-csrf-token');
				obj.tkn = header_xcsrf_token;
				obj.data = data;
				
//				/*
//				 * provisory for getting funclocs with mat pics
//				 */
//				getFunclocsWithMatpics();
				
				obj.subFuncLocLoad = true;				
				sap.ui.getCore().byId('initDetails--snapCancel').setVisible(true);		
				// add event to detail button
				sap.ui.getCore().byId('application-MasterBtn').setVisible(true);
				document.getElementById('initDetails--initPage-title').innerHTML = obj.tplnr;				
				console.log( obj.tplnr );
				sap.ui.getCore().byId("initDetails").getController().onFunclocRecognized( obj.data );
				// hide busy indicator
				busy.setVisible(false);
				busy.setBusy(false);
 
			},
			error: function(jqXHR, textStatus, errorThrown){
				sap.m.MessageToast.show("Error loading SubFuncLocs: " + textStatus + ", " + errorThrown, { 
					duration : 5000,
					my : "center",
					at : "center"
				});
			}
		});
	}	
};


crazyArchitectWorld.init();