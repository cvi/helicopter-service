sap.ui.localResources("program-resources");

/*
 * Global variables
 */
var oThis; // this instance
var obj = new Object(); // helping object
var storage = null; // Encrypted Storage plugin

/*######################################################################################################################### 
 * 												1. Define App Instances 
 *#########################################################################################################################
 */

// Splitapp
var oSplitApp = new sap.m.SplitApp("application", {
	initialDetail: "loginDetail",
});


var oLoginDetailPage = sap.ui.view({
	id: "loginDetail",
	viewName:"program-resources.views.loginDetails", 
	type:sap.ui.core.mvc.ViewType.JS });


/*######################################################################################################################### 
 * 												4. Add Pages to App Elements
 *#########################################################################################################################
 */

oSplitApp.addDetailPage(oLoginDetailPage);
oSplitApp.placeAt("content");
oSplitApp.setMode(sap.m.SplitAppMode.HideMode);