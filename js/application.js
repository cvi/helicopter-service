sap.ui.localResources("program-resources");

/*
 * Global variables
 */
var oThis; // this instance
var obj = new Object(); // helping object
var storage = null; // Encrypted storage plugin
//var dataset;	// for data after GET
//var position; // variables for list builds
//var reload; // variable to show if data was reloaded
//obj.root = "http://localhost:8080"; // root URI for requests

/*######################################################################################################################### 
 * 												1. Define App Instances 
 *#########################################################################################################################
 */

// Splitapp with masterview events
var oSplitApp = new sap.m.SplitApp("application", {
	initialDetail: "initDetails",
	initialMaster: "initMaster",
	beforeMasterOpen: function() {
		var tmp = oSplitApp.getCurrentDetailPage();
		if(tmp.sId == 'initDetails') {
			$("#tiles").hide();
		}
	},
	
	afterMasterClose: function() {
		if(!sap.ui.getCore().byId('initDetails--snapCancel').getVisible()) {
			$("#tiles").hide();
			obj.data = null;
		} else {
			var tmp = oSplitApp.getCurrentDetailPage();
			sap.ui.getCore().byId('createButton').setVisible(false);
			if(tmp.sId == 'initDetails') {
				$("#tiles").show();
				crazyArchitectWorld.removeModel(obj.data.d);
			}			
		}
	}
});

oSplitApp.setMode(sap.m.SplitAppMode.HideMode);

/*######################################################################################################################### 
 * 											2. Define placeholders for Master Views 
 *#########################################################################################################################
 */
var oInitMasterPage = sap.ui.view({
	id: "initMaster",
	viewName:"program-resources.views.initMaster", 
	type:sap.ui.core.mvc.ViewType.JS });

var oEquipMasterPage = sap.ui.view({
	id: "equipMaster",
	viewName:"program-resources.views.equipMaster", 
	type:sap.ui.core.mvc.ViewType.JS });

var oNotifMasterPage = sap.ui.view({
	id: "notifMaster",
	viewName:"program-resources.views.notifMaster", 
	type:sap.ui.core.mvc.ViewType.JS });

var oOrderMasterPage = sap.ui.view({
	id: "orderMaster",
	viewName:"program-resources.views.orderMaster", 
	type:sap.ui.core.mvc.ViewType.JS });

/*######################################################################################################################### 
 * 											3. Define placeholders for single and details views 
 *#########################################################################################################################
 */

var oInitDetailPage = sap.ui.view({
	id: "initDetails",
	viewName:"program-resources.views.initDetails", 
	type:sap.ui.core.mvc.ViewType.JS });

var oEquipDetailPage = sap.ui.view({
	id: "equipDetails",
	viewName:"program-resources.views.equipDetails", 
	type:sap.ui.core.mvc.ViewType.JS });

var oNotifDetailPage = sap.ui.view({
	id: "notifDetails",
	viewName:"program-resources.views.notifDetails", 
	type:sap.ui.core.mvc.ViewType.JS });

var oOrderDetailPage = sap.ui.view({
	id: "orderDetails",
	viewName:"program-resources.views.orderDetails", 
	type:sap.ui.core.mvc.ViewType.JS });

/*######################################################################################################################### 
 * 												4. Add Pages to App Elements
 *#########################################################################################################################
 */

oSplitApp.addMasterPage(oInitMasterPage).addMasterPage(oEquipMasterPage).addMasterPage(oNotifMasterPage).addMasterPage(oOrderMasterPage);
oSplitApp.addDetailPage(oInitDetailPage).addDetailPage(oEquipDetailPage).addDetailPage(oNotifDetailPage).addDetailPage(oOrderDetailPage);
oSplitApp.placeAt("content");